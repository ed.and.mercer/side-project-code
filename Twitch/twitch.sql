--minute_watched session builder + daily amounts
with stream as (
select event_type
  , min(client_time) over(partition by channel, context, session_number order by client_time asc) start_time
  , client_time end_time
  , session_number
  , minutes_watched
  , channel
from (
  select *
    , max(context) over(partition by channel, event_type, session_number) minutes_watched
  from (
    select *
      , sum(new_stream_flag) over(partition by channel, event_type order by client_time asc rows between unbounded preceding and current row) +1 session_number
    from (
      select *
        , if(lag(context) over(partition by channel, event_type order by client_time asc) > context,1,0) new_stream_flag
      from (
        select * except(context)
          , cast(context as int64) context
        from `twitch.twitch_raw`
        where event_type = 'minutewatched'
          ) clean
        ) stage1
    ) stage2
  ) stage3
where minutes_watched = context
)

select *
  , dense_rank() over(order by total_minutes_watched desc) stream_rank
from(
  select *
    , sum(minutes_watched_daily) over(partition by channel) total_minutes_watched
    , sum(minutes_watched_daily) over(partition by channel order by date rows between unbounded preceding and current row) cumulative_minutes_watched
    from (
    select date(start_time) date
      , channel
      , sum(minutes_watched) minutes_watched_daily
    from stream
    group by 1,2
    ) k
  ) z
order by 6 asc
