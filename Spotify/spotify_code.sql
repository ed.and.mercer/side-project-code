-- playlist cleaning
select count(distinct playlist_name)
from (
  select track.albumName album_name
    , track.artistname artist_name
    , track.trackname track_name
    , lastmodifieddate last_updated
    , name playlist_name
  from (
    select *
    from `my_spotify_data.playlist1`
     , unnest(playlists) as playlist
  ) z
  , unnest(items) as items
) k

-- playlist counts
with staging as
(
SELECT track.albumName album_name
  , track.artistname artist_name
  , track.trackname track_name
  , lastmodifieddate last_updated
  , name playlist_name
FROM (
  SELECT playlist.*
  FROM `my_spotify_data.playlist1`
   , UNNEST(playlists) AS playlist
  ) z
, UNNEST(items) AS items
WHERE track.albumname IS NOT null

)
select count(distinct track_name) count_tracks
  , count(distinct artist_name) count_artists
  , count(distinct album_name) count_albums
  , playlist_name
from staging
group by 4

-- playlist sum
WITH staging AS
(
SELECT track.albumName albumname
  , track.artistname artistname
  , track.trackname trackname
  , lastmodifieddate last_updated
  , name playlist_name
FROM (
  SELECT playlist.*
  FROM `my_spotify_data.playlist1`
   , UNNEST(playlists) AS playlist
  ) z
, UNNEST(items) AS items
WHERE track.albumname IS NOT null

)
SELECT sum(msplayed)/3600000 total_hours_played
  , playlist_name
FROM staging a
INNER JOIN`my_spotify_data.streaming_history*` b
ON a.trackname = b.trackname
GROUP BY 2
ORDER BY 1 DESC

############################
-- main datamart
with podcast_staging as (
  select msplayed/3600000 total_hours_played
    , msplayed/60000 total_minutes_played
    , stream.endtime date
    , lib.artistname artist
    , lib.albumname album
    , stream.trackname track
  from (
    SELECT shows.name albumname
      , shows.publisher artistname
    FROM `my_spotify_data.your_library`
      , UNNEST(shows) AS shows
     where name is not null
     )  lib
  inner join `my_spotify_data.streaming_history*` stream
  on lib.albumname = stream.artistname
),

podcast_join as (
  SELECT  shows.name album
    ,  shows.publisher artist
  FROM `my_spotify_data.your_library`
    , UNNEST(shows) AS shows
  WHERE name IS NOT null
  group by 1,2
),

song_length_stage as (
select track
  , artist
  , total_minutes_played  song_length_minutes
from (
  select *
    ,  rank() over(partition by track, artist order by freq desc) rank
  from (
    select count(*) freq
      , msplayed/60000 total_minutes_played
      , trackname track
      , artistname artist
    from `my_spotify_data.streaming_history*`
    group by 2,3,4
    ) _1
  ) _2
where rank = 1
),


main_staging as (
  select *
  from (
    select msplayed/3600000 total_hours_played
      , msplayed/60000 total_minutes_played
      , stream.endtime date
      , coalesce(stream.artistname,lib.artistname) artist
      , lib.albumname album
      , coalesce(stream.trackname,lib.trackname) track
    from (
    SELECT album albumname
          , artist artistname
          , track trackname
        FROM `my_spotify_data.your_library`
         , UNNEST(tracks) AS tracks
       )  lib
    right join `my_spotify_data.streaming_history*` stream
    using(artistname, trackname)
    where stream.artistname not in (select distinct album from podcast_join)
  )
),

final_staging as (
  select *
  from main_staging
  union all
  select *
  from podcast_staging
)


select *
  , round(sum(skip_check) over(partition by track, album, artist) / count(skip_check) over(partition by track, album, artist),2)*100 skip_percentage
  , case when track_length_minutes < 1 then 1 else 0 end short_song_check
  from (
  select * except(song_length)
    , if(total_minutes_played >= song_length or podcast_check = 1,0,1) skip_check
    , case when podcast_check = 1 then sum(total_minutes_played) over(partition by track)
      else song_length
      end track_length_minutes
  from (
    select k.*
      , if(p.artist is not null or artist in ('Critical Role', 'Stuff You Should Know', 'Off Menu with Ed Gamble and James Acaster'),1,0) podcast_check
      , case when day_of_week = 1 then 'Monday'
        when day_of_week = 2 then 'Tuesday'
        when day_of_week = 3 then 'Wednesday'
        when day_of_week = 4 then 'Thursday'
        when day_of_week = 5 then 'Friday'
        when day_of_week = 6 then 'Saturday'
        when day_of_week = 7 then 'Sunday'
        end weekday
    from (
      select * except (date, track)
        , ltrim(track) track
        , case when date < '2020-03-11' then 'pre'
          else 'post'
          end lockdown
        , date
        , cast(extract(hour from date) as string) hour_
        , extract(dayofweek from date) day_of_week
        , sl.song_length_minutes song_length
      from final_staging s
      left join song_length_stage sl
      using (artist, track)
      ) k
    left join podcast_join p
    using(artist, album)
    where track != 'Unknown Track'
      and artist != 'Unknown Artist'
    ) f
  ) z

############################
-- datamart_2
select *
from (
  select avg(playtime_min) over(partition by weekday, day_of_week, hour_, lockdown ) avg_playtime_min
    , avg(daily_total) over(partition by weekday, lockdown) daily_avg_hours
    , weekday
    , day_of_week
    , hour_ hour
    , lockdown
  from (
  select *
    , sum(playtime_min) over(partition by date, weekday, lockdown) /60 daily_total
    from (
    select sum(total_minutes_played) playtime_min
      , date(date) date
      , weekday
      , day_of_week
      , hour_
      , lockdown
    from `my_spotify_data.datamart_1`
    where date(date) between date_sub('2020-03-11',interval 100 day) and date_add('2020-03-11',interval 100 day)
    group by 2,3,4,5,6
    ) z
  ) s
)fix
group by 1,2,3,4,5,6

############################
-- datamart_3
select * except (value)
  , case when lockdown = 'pre' then value*-1 else value end value
  , case when lockdown = 'pre' then value else value end value_label
from (
-- total listening in days
select  round(sum(total_minutes_played)/1440,2) value
  , 'days listened' as value_name
  , lockdown
from `my_spotify_data.datamart_1`
where date(date) between date_sub('2020-03-11',interval 100 day) and date_add('2020-03-11',interval 100 day)
group by 2,3

union all
-- distinct artists
select count(*) value
  , 'artist_count' as value_name
  , lockdown
from (
select distinct artist
  , min(date) over(partition by artist) first_date
  , lockdown
from `my_spotify_data.datamart_1`
)
where date(first_date) between date_sub('2020-03-11',interval 100 day) and date_add('2020-03-11',interval 100 day)
group by 2,3

union all
-- daily avg hours
select avg(value) value
  , 'daily_hours_average' as value_name
  , lockdown
from (
select  round(sum(total_minutes_played)/60,2) value
  , lockdown
  , date(date)
from `my_spotify_data.datamart_1`
where date(date) between date_sub('2020-03-11',interval 100 day) and date_add('2020-03-11',interval 100 day)
group by 2,3
)
group by 2,3
)

############################
-- datamart_4
with stage as (
  select *
    , rank() over(order by sum_new_track desc) song_rank
    , dense_rank() over(order by sum_new_artist desc) artist_rank
  FROM (
    select sum(total_hours_played) over(partition by track) sum_new_track
      , sum(total_hours_played) over(partition by artist) sum_new_artist
      , sum(total_hours_played) over() sum_total
      , track
      , artist
    FROM `my_spotify_data.datamart_1`
    )z
  group by 1,2,3,4,5
),

artist_10 as (
select 'artist' as type
  , round(sum(summ)/sum_total *100,2) percent_
from (
  select distinct artist
    , min(sum_new_artist) summ
    , sum_total
  from stage
  where artist_rank <= 10
  group by 1,3
  )
group by 1, sum_total
),

song_10 as (
select 'song' as type
  , round(sum(summ)/sum_total *100,2) percent_
from (
  select distinct track
    , min(sum_new_track) summ
    , sum_total
  from stage
  where song_rank <= 100
  group by 1,3
  )
group by 1, sum_total
)

select *
from song_10
union all
select *
from artist_10
