select * except(days_listened,artist)
  , "milo" as artist
  , sum(days_listened) days_listened
from (
  select round(sum(ms_played)/8.64e+7,2) days_listened
    , master_metadata_album_artist_name  artist
    , 'all' as year
  from `electric-glyph-220814.spotify_full.endsong_*`
  group by 2
  union all
  select round(sum(ms_played)/8.64e+7,2) days_listened
    , master_metadata_album_artist_name
    , cast(extract(year from ts) as string)
  from `electric-glyph-220814.spotify_full.endsong_*`
  group by 2,3
  )
  where artist like "%Milo%"
    or artist like "%Nostrum Grocers%"
    or artist like "%Scallops Hotel%"
    or artist like "%Ruby Yacht%"
    or artist like "%R.A.P. Ferriera%"
  or artist in ("Scallops Hotel","Nostrum Grocers", "Ruby Yacht","R.A.P. Ferriera")
group by artist, year
order by 1 desc
